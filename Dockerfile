# stage 1
FROM node:latest as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm build --prod

# stage 2
FROM nginx:alpine
COPY --from=node /app/dist/angular-first-app /usr/share/nginx/html